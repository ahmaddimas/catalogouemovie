package id.co.wis.cataloguemovieextended.adapter;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Binder;
import android.os.Bundle;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import id.co.wis.cataloguemovieextended.R;
import id.co.wis.cataloguemovieextended.db.model.MovieContract;
import id.co.wis.cataloguemovieextended.model.Movie;
import id.co.wis.cataloguemovieextended.utils.Constants;

public class StackRemoteViewsFactory implements RemoteViewsService.RemoteViewsFactory {

    private Cursor movieCursor;
    private List<Movie> movieItems = new ArrayList<>();
    private Context mContext;
    private int mAppWidgetId;

    public StackRemoteViewsFactory(Context context, Intent intent) {
        mContext = context;
        mAppWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onDataSetChanged() {
        final long identityToken = Binder.clearCallingIdentity();

        movieCursor = mContext.getContentResolver().query(MovieContract.CONTENT_URI, null, null, null, null);
        if (movieCursor != null) {
            movieCursor.moveToFirst();
            Movie movie;
            while (!movieCursor.isAfterLast()) {
                movie = new Movie(movieCursor);
                movieItems.add(movie);
                movieCursor.moveToNext();
            }
        }

        Binder.restoreCallingIdentity(identityToken);
    }

    @Override
    public void onDestroy() {

    }

    @Override
    public int getCount() {
        int count = 0;
        if (movieCursor != null)
            count = movieCursor.getCount();
        return count;
    }

    @Override
    public RemoteViews getViewAt(int position) {
        RemoteViews remoteViews = new RemoteViews(mContext.getPackageName(), R.layout.widget_item);

        Movie movie = movieItems.get(position);
        Bitmap bitmap = null;
        try {
            RequestOptions requestOptions = new RequestOptions().placeholder(R.drawable.ic_image).error(R.drawable.ic_broken_image);
            bitmap = Glide.with(mContext)
                    .asBitmap()
                    .load(Constants.IMAGE_BACKDROP_BASE_URL + movie.getBackdrop_path())
                    .apply(requestOptions)
                    .submit(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL).get();
        }catch (InterruptedException | ExecutionException e){
            Log.d("Widget Load Error","error");
        }
        remoteViews.setImageViewBitmap(R.id.cover_image, bitmap);
        remoteViews.setTextViewText(R.id.title_text, movie.getTitle());

        Bundle extras = new Bundle();
        extras.putInt(Constants.MOVIE_ITEM_KEY, movie.getId());
        Intent fillInIntent = new Intent();
        fillInIntent.putExtras(extras);

        remoteViews.setOnClickFillInIntent(R.id.cover_image, fillInIntent);
        return remoteViews;
    }

    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }
}
