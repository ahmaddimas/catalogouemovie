package id.co.wis.cataloguemovieextended.db.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import id.co.wis.cataloguemovieextended.db.model.MovieContract;
import id.co.wis.cataloguemovieextended.model.Movie;

public class MovieHelper {

    private Context context;
    private DatabaseHelper databaseHelper;

    private SQLiteDatabase database;

    public MovieHelper(Context context) {
        this.context = context;
    }

    public MovieHelper open() throws SQLException {
        databaseHelper = new DatabaseHelper(context);
        database = databaseHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        databaseHelper.close();
    }

    public ArrayList<Movie> findAll() {
        Cursor cursor = database.rawQuery("select * from "+ MovieContract.TABLE +" order by "+ MovieContract.EntityColumns._ID +" asc", null);
        cursor.moveToFirst();
        ArrayList<Movie> list = new ArrayList<>();
        Movie movie;
        if (cursor.getCount() > 0) {
            do {
                movie = new Movie();
                movie.setId(cursor.getInt(cursor.getColumnIndexOrThrow(MovieContract.EntityColumns._ID)));
                movie.setTitle(cursor.getString(cursor.getColumnIndexOrThrow(MovieContract.EntityColumns.title)));
                movie.setRelease_date(cursor.getString(cursor.getColumnIndexOrThrow(MovieContract.EntityColumns.releaseDate)));
                movie.setPoster_path(cursor.getString(cursor.getColumnIndexOrThrow(MovieContract.EntityColumns.posterPath)));
                movie.setBackdrop_path(cursor.getString(cursor.getColumnIndexOrThrow(MovieContract.EntityColumns.backdropPath)));
                movie.setOverview(cursor.getString(cursor.getColumnIndexOrThrow(MovieContract.EntityColumns.overview)));
                movie.setPopularity(cursor.getFloat(cursor.getColumnIndexOrThrow(MovieContract.EntityColumns.popularity)));

                list.add(movie);
                cursor.moveToNext();
            } while (!cursor.isAfterLast());
        }
        cursor.close();
        return list;
    }

    public long insert(Movie movie) {
        ContentValues contentValues = new ContentValues();

        contentValues.put(MovieContract.EntityColumns._ID, movie.getId());
        contentValues.put(MovieContract.EntityColumns.title, movie.getTitle());
        contentValues.put(MovieContract.EntityColumns.releaseDate, movie.getRelease_date());
        contentValues.put(MovieContract.EntityColumns.posterPath, movie.getPoster_path());
        contentValues.put(MovieContract.EntityColumns.backdropPath, movie.getBackdrop_path());
        contentValues.put(MovieContract.EntityColumns.overview, movie.getOverview());
        contentValues.put(MovieContract.EntityColumns.popularity, movie.getPopularity());
        return database.insert(MovieContract.TABLE, null, contentValues);
    }

    public int delete(int id) {
        return database.delete(MovieContract.TABLE, MovieContract.EntityColumns._ID + " = '"+ id +"'", null);
    }

    public Cursor findByIdProvider(String id) {
        return database.query(MovieContract.TABLE, null,
                MovieContract.EntityColumns._ID + " = ?", new String[]{id},
                null, null, MovieContract.EntityColumns._ID +" ASC");
    }

    public Cursor findAllProvider() {
        return database.query(MovieContract.TABLE, null, null, null, null, null, MovieContract.EntityColumns._ID +" ASC");
    }

    public long insertProvider(ContentValues values) {
        return database.insert(MovieContract.TABLE, null, values);
    }

    public int deleteProvider(String id) {
        return database.delete(MovieContract.TABLE, MovieContract.EntityColumns._ID + " = ?", new String[]{id});
    }
}
