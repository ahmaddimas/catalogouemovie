package id.co.wis.cataloguemovieextended.utils;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.AsyncTaskLoader;

import java.io.IOException;
import java.util.ArrayList;

import id.co.wis.cataloguemovieextended.model.Movie;
import id.co.wis.cataloguemovieextended.model.MoviesResponse;
import retrofit2.Call;

public class MovieLoader extends AsyncTaskLoader<ArrayList<Movie>> {

    private Call<MoviesResponse> call;

    public MovieLoader(@NonNull Context context, Call<MoviesResponse> call) {
        super(context);
        this.call = call;
    }

    @Nullable
    @Override
    public ArrayList<Movie> loadInBackground() {
        ArrayList<Movie> movies = new ArrayList<>();

        try {
            movies.addAll(call.execute().body().getListMovie());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return movies;
    }
}
