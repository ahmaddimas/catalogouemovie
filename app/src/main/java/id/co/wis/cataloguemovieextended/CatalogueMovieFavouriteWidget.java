package id.co.wis.cataloguemovieextended;

import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.widget.RemoteViews;

import id.co.wis.cataloguemovieextended.db.model.MovieContract;
import id.co.wis.cataloguemovieextended.model.Movie;
import id.co.wis.cataloguemovieextended.services.StackWidgetService;
import id.co.wis.cataloguemovieextended.utils.Constants;

/**
 * Implementation of App Widget functionality.
 */
public class CatalogueMovieFavouriteWidget extends AppWidgetProvider {

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {

        Intent intent = new Intent(context, StackWidgetService.class);
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
        intent.setData(Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME)));

        // Construct the RemoteViews object
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.catalogue_movie_favourite_widget);
        views.setRemoteAdapter(R.id.stack_view, intent);
        views.setEmptyView(R.id.stack_view, R.id.empty_view);

        Intent actionIntent = new Intent(context, CatalogueMovieFavouriteWidget.class);
        actionIntent.setAction(Constants.OPEN_ACTION_KEY);
        actionIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);

        intent.setData(Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME)));

        PendingIntent actionPendingIntent = PendingIntent.getBroadcast(context, 0, actionIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        views.setPendingIntentTemplate(R.id.stack_view, actionPendingIntent);

        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @SuppressLint("Recycle")
    @Override
    public void onReceive(Context context, Intent intent) {
        AppWidgetManager widgetManager = AppWidgetManager.getInstance(context);

        if (intent.getAction().equals(Constants.OPEN_ACTION_KEY)) {
            int appWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
            int movieId = intent.getIntExtra(Constants.MOVIE_ITEM_KEY, 0);
            Movie movie = null;

            Uri uri = Uri.parse(MovieContract.CONTENT_URI +"/"+ movieId);
            Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToFirst();
                movie = new Movie(cursor);
                cursor.close();
            }

            Intent detailIntent = new Intent(context, DetailActivity.class);
            detailIntent.putExtra("MOVIE", movie);
            detailIntent.setData(uri);
            context.startActivity(detailIntent);
        }

        super.onReceive(context, intent);
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }
}

