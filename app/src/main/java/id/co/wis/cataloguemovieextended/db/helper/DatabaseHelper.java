package id.co.wis.cataloguemovieextended.db.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import id.co.wis.cataloguemovieextended.db.model.MovieContract;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static String DB_NAME = "db_movie";

    private static final int DB_VERSION = 1;

    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);

    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table "+ MovieContract.TABLE
                +" ("+ MovieContract.EntityColumns._ID +" integer primary key autoincrement, "
                + MovieContract.EntityColumns.title +" varchar(200) not null, "
                + MovieContract.EntityColumns.releaseDate +" text not null, "
                + MovieContract.EntityColumns.posterPath +" text not null, "
                + MovieContract.EntityColumns.backdropPath +" text not null, "
                + MovieContract.EntityColumns.overview +" text not null, "
                + MovieContract.EntityColumns.popularity +" double not null);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists "+ MovieContract.TABLE);
        onCreate(db);
    }
}
