package id.co.wis.cataloguemovieextended.fragments;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.wis.cataloguemovieextended.BuildConfig;
import id.co.wis.cataloguemovieextended.DetailActivity;
import id.co.wis.cataloguemovieextended.R;
import id.co.wis.cataloguemovieextended.adapter.MovieAdapter;
import id.co.wis.cataloguemovieextended.db.model.MovieContract;
import id.co.wis.cataloguemovieextended.model.Movie;
import id.co.wis.cataloguemovieextended.model.MoviesResponse;
import id.co.wis.cataloguemovieextended.services.ApiClient;
import id.co.wis.cataloguemovieextended.services.ApiServices;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends BottomSheetDialogFragment implements MovieAdapter.OnItemClickListener {

    @BindView(R.id.et_input)
    EditText etInput;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    ArrayList<Movie> listMovie = new ArrayList<>();
    MovieAdapter movieAdapter;

    public SearchFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        ButterKnife.bind(this, view);

        movieAdapter = new MovieAdapter(getActivity(), listMovie, this);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(movieAdapter);

        return view;
    }

    @OnClick(R.id.btn_search)
    public void onClick(Button button) {
        ApiServices apiServices = ApiClient.newInstance().create(ApiServices.class);
        Call<MoviesResponse> call = apiServices.searchMovies(BuildConfig.TmdbApiKey, etInput.getText().toString());

        call.enqueue(new Callback<MoviesResponse>() {
            @Override
            public void onResponse(Call<MoviesResponse> call, Response<MoviesResponse> response) {
                if (response.body() != null) {
                    if (response.body().getListMovie().size() > 0) {
                        listMovie.clear();
                        listMovie.addAll(response.body().getListMovie());
                        movieAdapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFailure(Call<MoviesResponse> call, Throwable t) {
                Toast.makeText(getActivity(), "Some error occured", Toast.LENGTH_SHORT).show();
                Log.d("SearchMovieException", "onFailure: "+ t.getMessage());
            }
        });
    }

    @Override
    public void onItemClick(Movie movie) {
        Uri uri = Uri.parse(MovieContract.CONTENT_URI +"/"+ movie.getId());

        Intent intent = new Intent(getActivity(), DetailActivity.class);
        intent.putExtra("MOVIE", movie);
        intent.setData(uri);
        startActivity(intent);
    }
}
