package id.co.wis.cataloguemovieextended.services;

import id.co.wis.cataloguemovieextended.model.MoviesResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiServices {

    @GET("discover/movie")
    Call<MoviesResponse> retrieveMovie(@Query("api_key") String key);

    @GET("search/movie")
    Call<MoviesResponse> searchMovies(@Query("api_key") String key, @Query("query") String query);

    @GET("movie/now_playing")
    Call<MoviesResponse> retrieveNowPlaying(@Query("api_key") String key);

    @GET("movie/upcoming")
    Call<MoviesResponse> retrieveUpcoming(@Query("api_key") String key);
}
