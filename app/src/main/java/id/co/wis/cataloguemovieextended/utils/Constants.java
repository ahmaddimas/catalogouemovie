package id.co.wis.cataloguemovieextended.utils;

public class Constants {
    public static final String BASE_URL = "https://api.themoviedb.org/3/";
    public static final String IMAGE_BASE_URL = "https://image.tmdb.org/t/p/w185_and_h278_bestv2/";
    public static final String IMAGE_BACKDROP_BASE_URL = "https://image.tmdb.org/t/p/w350_and_h196_bestv2/";

    public static final String MOVIE_ITEM_KEY = "id.co.wis.cataloguemovieextended.MovieItem";
    public static final String OPEN_ACTION_KEY = "id.co.wis.cataloguemovieextended.action.Open";

    public static final String CHANNEL_NOTIF = "id.co.wis.cataloguemovieextended.Notification";
    public static final String CHANNEL_NOTIF_NAME = "Extended Notification";
    public static final String REMINDER_NOTIF = "id.co.wis.cataloguemovieextended.notification.Reminder";
    public static final String RELEASE_NOTIF = "id.co.wis.cataloguemovieextended.notification.Release";
}
