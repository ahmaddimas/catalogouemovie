package id.co.wis.cataloguemovieextended.model;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import id.co.wis.cataloguemovieextended.db.model.MovieContract;

public class Movie implements Parcelable {

    int id;
    String title, release_date, poster_path, backdrop_path, overview;
    double popularity;

    public Movie() {
    }

    public Movie(Cursor cursor) {
        this.id = MovieContract.getColumnInt(cursor, MovieContract.EntityColumns._ID);
        this.title = MovieContract.getColumnString(cursor, MovieContract.EntityColumns.title);
        this.release_date = MovieContract.getColumnString(cursor, MovieContract.EntityColumns.releaseDate);
        this.poster_path = MovieContract.getColumnString(cursor, MovieContract.EntityColumns.posterPath);
        this.backdrop_path = MovieContract.getColumnString(cursor, MovieContract.EntityColumns.backdropPath);
        this.overview = MovieContract.getColumnString(cursor, MovieContract.EntityColumns.overview);
        this.popularity = MovieContract.getColumnDouble(cursor, MovieContract.EntityColumns.popularity);
    }

    protected Movie(Parcel in) {
        id = in.readInt();
        title = in.readString();
        release_date = in.readString();
        poster_path = in.readString();
        backdrop_path = in.readString();
        overview = in.readString();
        popularity = in.readFloat();
    }

    public static final Creator<Movie> CREATOR = new Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel in) {
            return new Movie(in);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRelease_date() {
        return release_date;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }

    public String getPoster_path() {
        return poster_path;
    }

    public void setPoster_path(String poster_path) {
        this.poster_path = poster_path;
    }

    public String getBackdrop_path() {
        return backdrop_path;
    }

    public void setBackdrop_path(String backdrop_path) {
        this.backdrop_path = backdrop_path;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public double getPopularity() {
        return popularity;
    }

    public void setPopularity(double popularity) {
        this.popularity = popularity;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(title);
        dest.writeString(release_date);
        dest.writeString(poster_path);
        dest.writeString(backdrop_path);
        dest.writeString(overview);
        dest.writeDouble(popularity);
    }
}
