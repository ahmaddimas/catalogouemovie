package id.co.wis.cataloguemovieextended.receiver;

import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import id.co.wis.cataloguemovieextended.BuildConfig;
import id.co.wis.cataloguemovieextended.DetailActivity;
import id.co.wis.cataloguemovieextended.MainActivity;
import id.co.wis.cataloguemovieextended.R;
import id.co.wis.cataloguemovieextended.db.model.MovieContract;
import id.co.wis.cataloguemovieextended.helper.DateHelper;
import id.co.wis.cataloguemovieextended.model.Movie;
import id.co.wis.cataloguemovieextended.model.MoviesResponse;
import id.co.wis.cataloguemovieextended.services.ApiClient;
import id.co.wis.cataloguemovieextended.services.ApiServices;
import id.co.wis.cataloguemovieextended.utils.Constants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationReceiver extends BroadcastReceiver {

    public static final int REMINDER_ID = 1;
    public static final int RELEASE_ID = 2;

    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent.getAction().equals(Constants.REMINDER_NOTIF)) {
            Intent detailIntent = new Intent(context, MainActivity.class);

            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, detailIntent, 0);

            showNotification(context, REMINDER_ID, pendingIntent,
                            context.getResources().getString(R.string.app_name),
                            context.getResources().getString(R.string.reminder_text));
        } else if (intent.getAction().equals(Constants.RELEASE_NOTIF)) {
            final Context mContext = context;
            Callback<MoviesResponse> callback = new Callback<MoviesResponse>() {
                @Override
                public void onResponse(@NonNull Call<MoviesResponse> call, @NonNull Response<MoviesResponse> response) {
                    if (response.body() != null) {
                        if (response.body().getListMovie().size() > 0) {
                            List<Movie> movieList = new ArrayList<>(response.body().getListMovie());

                            if (movieList.size() > 0) {
                                Calendar today  = Calendar.getInstance();
                                today.setTime(new Date());
                                for (Movie movie : movieList) {
                                    Date releaseDate = DateHelper.parseDate("yyyy-MM-dd", "yyyy-MM-dd", movie.getRelease_date());
                                    Calendar release  = Calendar.getInstance();
                                    release.setTime(releaseDate);

                                    boolean sameDay = today.get(Calendar.DAY_OF_YEAR) == release.get(Calendar.DAY_OF_YEAR) && today.get(Calendar.YEAR) == release.get(Calendar.YEAR);

                                    if (sameDay) {
                                        Uri uri = Uri.parse(MovieContract.CONTENT_URI +"/"+ movie.getId());

                                        Intent detailIntent = new Intent(mContext, DetailActivity.class);
                                        detailIntent.putExtra("MOVIE", movie);
                                        detailIntent.setData(uri);

                                        PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0, detailIntent, 0);

                                        showNotification(mContext, RELEASE_ID, pendingIntent, movie.getTitle(), movie.getTitle().concat(" release"));
                                    }
                                }
                            }
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<MoviesResponse> call, @NonNull Throwable t) {
                    Log.d("MovieException", "onFailure: "+ t.getMessage());
                }
            };

            fetchMovie(callback);
        }
    }

    private void fetchMovie(Callback<MoviesResponse> callback) {
        ApiServices apiServices = ApiClient.newInstance().create(ApiServices.class);

        Call<MoviesResponse> call = apiServices.retrieveNowPlaying(BuildConfig.TmdbApiKey);

        call.enqueue(callback);
    }

    private void showNotification(Context context, int notifId, PendingIntent pendingIntent, String title, String text) {
        Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder builder = new NotificationCompat
                .Builder(context, Constants.CHANNEL_NOTIF)
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher))
                .setContentTitle(title)
                .setContentText(text)
                .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                .setSound(sound)
                .setAutoCancel(true);

        NotificationManager notificationManagerCompat = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            NotificationChannel channel = new NotificationChannel(Constants.CHANNEL_NOTIF,
                    Constants.CHANNEL_NOTIF_NAME,
                    NotificationManager.IMPORTANCE_DEFAULT);

            channel.enableVibration(true);
            channel.setVibrationPattern(new long[]{1000, 1000, 1000, 1000, 1000});

            builder.setChannelId(Constants.CHANNEL_NOTIF);

            if (notificationManagerCompat != null) {
                notificationManagerCompat.createNotificationChannel(channel);
            }
        }

        if (notificationManagerCompat != null) {
            notificationManagerCompat.notify(notifId, builder.build());
        }
    }

    public static void setDailyReminder(Context context) {
        Calendar time = Calendar.getInstance();
        time.set(Calendar.HOUR_OF_DAY, 23);
        time.set(Calendar.MINUTE, 23);
        time.set(Calendar.SECOND, 0);

        Intent notifIntent = new Intent(context, NotificationReceiver.class);
        notifIntent.setAction(Constants.REMINDER_NOTIF);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, REMINDER_ID, notifIntent, 0);

        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        if (manager != null) {
            manager.setInexactRepeating(AlarmManager.RTC_WAKEUP, time.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
        }
    }

    public static void setReleaseReminder(Context context) {
        Calendar time = Calendar.getInstance();
        time.set(Calendar.HOUR_OF_DAY, 23);
        time.set(Calendar.MINUTE, 23);
        time.set(Calendar.SECOND, 0);

        Intent notifIntent = new Intent(context, NotificationReceiver.class);
        notifIntent.setAction(Constants.RELEASE_NOTIF);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, RELEASE_ID, notifIntent, 0);

        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        if (manager != null) {
            manager.setInexactRepeating(AlarmManager.RTC_WAKEUP, time.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
        }
    }

    public static void cancelReminder(Context context, int reminderId) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent notifIntent = new Intent(context, NotificationReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, reminderId, notifIntent, 0);

        pendingIntent.cancel();

        if (alarmManager != null) {
            alarmManager.cancel(pendingIntent);
            Toast.makeText(context, "cancel mag: ha", Toast.LENGTH_SHORT).show();
        }
        Toast.makeText(context, "cancel: ha", Toast.LENGTH_SHORT).show();
    }

    public static boolean isReminderExist(Context context, int reminderId) {
        Intent notifIntent = new Intent(context, NotificationReceiver.class);

        return PendingIntent.getBroadcast(context, reminderId, notifIntent, PendingIntent.FLAG_NO_CREATE) != null;
    }
}
