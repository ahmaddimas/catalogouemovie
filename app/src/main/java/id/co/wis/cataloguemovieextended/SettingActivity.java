package id.co.wis.cataloguemovieextended;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.wis.cataloguemovieextended.receiver.NotificationReceiver;

public class SettingActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {

    @BindView(R.id.reminder_switch)
    Switch reminderSwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        ButterKnife.bind(this);
        setTitle(getResources().getString(R.string.action_settings));

        reminderSwitch.setOnCheckedChangeListener(this);

        boolean checked = NotificationReceiver.isReminderExist(this, NotificationReceiver.REMINDER_ID)
                || NotificationReceiver.isReminderExist(this, NotificationReceiver.RELEASE_ID);

        Toast.makeText(this, "reminder: " + NotificationReceiver.isReminderExist(this, NotificationReceiver.REMINDER_ID), Toast.LENGTH_SHORT).show();
        Toast.makeText(this, "release: " + NotificationReceiver.isReminderExist(this, NotificationReceiver.RELEASE_ID), Toast.LENGTH_SHORT).show();

        reminderSwitch.setChecked(checked);
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
        boolean checked = NotificationReceiver.isReminderExist(this, NotificationReceiver.REMINDER_ID)
                || NotificationReceiver.isReminderExist(this, NotificationReceiver.RELEASE_ID);
        if (isChecked) {
            if (!checked) {
                Toast.makeText(this, "Daily Reminder setup", Toast.LENGTH_SHORT).show();
                NotificationReceiver.setDailyReminder(this);
                NotificationReceiver.setReleaseReminder(this);
            } else {
                Toast.makeText(this, "check false", Toast.LENGTH_SHORT).show();
            }
        } else {
            NotificationReceiver.cancelReminder(this, NotificationReceiver.REMINDER_ID);
            NotificationReceiver.cancelReminder(this, NotificationReceiver.RELEASE_ID);
        }
    }
}
