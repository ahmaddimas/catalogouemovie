package id.co.wis.cataloguemovieextended.services;

import android.content.Intent;
import android.widget.RemoteViewsService;

import id.co.wis.cataloguemovieextended.adapter.StackRemoteViewsFactory;

public class StackWidgetService extends RemoteViewsService {

    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        return new StackRemoteViewsFactory(this.getApplicationContext(), intent);
    }
}
