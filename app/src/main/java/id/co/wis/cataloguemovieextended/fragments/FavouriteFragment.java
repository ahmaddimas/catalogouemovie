package id.co.wis.cataloguemovieextended.fragments;


import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.wis.cataloguemovieextended.DetailActivity;
import id.co.wis.cataloguemovieextended.R;
import id.co.wis.cataloguemovieextended.adapter.MovieAdapter;
import id.co.wis.cataloguemovieextended.db.model.MovieContract;
import id.co.wis.cataloguemovieextended.model.Movie;

/**
 * A simple {@link Fragment} subclass.
 */
public class FavouriteFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>, MovieAdapter.OnItemClickListener {

    private final int LOADER_ID = 137;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    Cursor movieCursor;
    MovieAdapter movieAdapter;

    public FavouriteFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_favourite, container, false);
        ButterKnife.bind(this, view);

        movieAdapter = new MovieAdapter(getActivity(), movieCursor, this);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(movieAdapter);

        getActivity().getSupportLoaderManager().initLoader(LOADER_ID, null, this);

        return view;
    }

    @Override
    public void onItemClick(Movie movie) {
        Uri uri = Uri.parse(MovieContract.CONTENT_URI +"/"+ movie.getId());

        Intent intent = new Intent(getActivity(), DetailActivity.class);
        intent.putExtra("MOVIE", movie);
        intent.setData(uri);
        startActivity(intent);
    }

    @NonNull
    @Override
    public Loader<Cursor> onCreateLoader(int id, @Nullable Bundle args) {
        return new CursorLoader(getActivity(), MovieContract.CONTENT_URI, null, null, null, null);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor data) {
        movieAdapter.setMovieCursor(data);
        movieAdapter.notifyDataSetChanged();
    }

    @Override
    public void onLoaderReset(@NonNull Loader<Cursor> loader) {
        movieAdapter.setMovieCursor(null);
        movieAdapter.notifyDataSetChanged();
    }
}
