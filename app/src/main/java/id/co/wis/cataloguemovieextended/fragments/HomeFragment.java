package id.co.wis.cataloguemovieextended.fragments;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.wis.cataloguemovieextended.BuildConfig;
import id.co.wis.cataloguemovieextended.DetailActivity;
import id.co.wis.cataloguemovieextended.R;
import id.co.wis.cataloguemovieextended.adapter.MovieAdapter;
import id.co.wis.cataloguemovieextended.db.model.MovieContract;
import id.co.wis.cataloguemovieextended.model.Movie;
import id.co.wis.cataloguemovieextended.model.MoviesResponse;
import id.co.wis.cataloguemovieextended.services.ApiClient;
import id.co.wis.cataloguemovieextended.services.ApiServices;
import id.co.wis.cataloguemovieextended.utils.MovieLoader;
import retrofit2.Call;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements LoaderManager.LoaderCallbacks<ArrayList<Movie>>, MovieAdapter.OnItemClickListener {

    private final int LOADER_ID = 133;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    ArrayList<Movie> listMovie = new ArrayList<>();
    MovieAdapter movieAdapter;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);

        movieAdapter = new MovieAdapter(getActivity(), listMovie, this);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(movieAdapter);

        getActivity().getSupportLoaderManager().initLoader(LOADER_ID, null, this);

        return view;
    }

    @Override
    public void onItemClick(Movie movie) {
        Uri uri = Uri.parse(MovieContract.CONTENT_URI +"/"+ movie.getId());

        Intent intent = new Intent(getActivity(), DetailActivity.class);
        intent.putExtra("MOVIE", movie);
        intent.setData(uri);
        startActivity(intent);
    }

    @NonNull
    @Override
    public Loader<ArrayList<Movie>> onCreateLoader(int id, @Nullable Bundle args) {
        ApiServices apiServices = ApiClient.newInstance().create(ApiServices.class);
        Call<MoviesResponse> call = apiServices.retrieveMovie(BuildConfig.TmdbApiKey);
        MovieLoader loader = new MovieLoader(getActivity(), call);
        loader.forceLoad();
        return loader;
    }

    @Override
    public void onLoadFinished(@NonNull Loader<ArrayList<Movie>> loader, ArrayList<Movie> data) {
        movieAdapter.setMovieList(data);
        movieAdapter.notifyDataSetChanged();
    }

    @Override
    public void onLoaderReset(@NonNull Loader<ArrayList<Movie>> loader) {
        movieAdapter.setMovieList(null);
        movieAdapter.notifyDataSetChanged();
    }
}
