package id.co.wis.cataloguemovieextended.db.model;

import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;

public class MovieContract {

    public static String TABLE = "favourite";

    public static final class EntityColumns implements BaseColumns {
        public static String title = "title";
        public static String releaseDate = "release_date";
        public static String posterPath = "poster_path";
        public static String backdropPath = "backdrop_path";
        public static String overview = "overview";
        public static String popularity = "popularity";
    }

    public static final String AUTHORITY = "id.co.wis.cataloguemovieextended";

    public static final Uri CONTENT_URI = new Uri.Builder()
            .scheme("content")
            .authority(AUTHORITY)
            .appendPath(TABLE)
            .build();

    public static String getColumnString(Cursor cursor, String columnName) {
        return cursor.getString(cursor.getColumnIndex(columnName));
    }

    public static int getColumnInt(Cursor cursor, String columnName) {
        return cursor.getInt(cursor.getColumnIndex(columnName));
    }

    public static double getColumnDouble(Cursor cursor, String columnName) {
        return cursor.getDouble(cursor.getColumnIndex(columnName));
    }
}
