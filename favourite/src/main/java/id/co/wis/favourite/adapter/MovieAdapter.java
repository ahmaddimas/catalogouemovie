package id.co.wis.favourite.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.wis.favourite.R;
import id.co.wis.favourite.helper.DateHelper;
import id.co.wis.favourite.model.Movie;
import id.co.wis.favourite.utils.Constants;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MovieViewHolder> {

    public interface OnItemClickListener{
        void onItemClick(Movie movie);
    }

    private Context context;
    private Cursor movieCursor;
    private OnItemClickListener listener;

    public MovieAdapter(Context context, Cursor movieCursor, OnItemClickListener listener) {
        this.context = context;
        this.movieCursor = movieCursor;
        this.listener = listener;
    }

    public void setMovieCursor(Cursor movieCursor) {
        this.movieCursor = movieCursor;
    }

    @NonNull
    @Override
    public MovieViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.movie_list, parent, false);
        return new MovieAdapter.MovieViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieViewHolder holder, final int position) {
        final Movie movie = getItem(position);

        String date = movie.getRelease_date().equalsIgnoreCase("") ? "Day Month Year" : DateHelper.formatDate("yyyy-MM-dd", "EEEE, MMM d, yyyy", movie.getRelease_date());

        RequestOptions requestOptions = new RequestOptions().placeholder(R.drawable.ic_image).error(R.drawable.ic_broken_image);
        Glide.with(context).load(Constants.IMAGE_BASE_URL + movie.getPoster_path()).apply(requestOptions).into(holder.ivCover);

        holder.tvTitle.setText(movie.getTitle());
        holder.tvOverview.setText(movie.getOverview());
        holder.tvDate.setText(date);

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(movie);
            }
        });
    }

    @Override
    public int getItemCount() {
        int count = 0;
        if (movieCursor != null)
            count = movieCursor.getCount();
        return count;
    }

    private Movie getItem(int position) {
        if (!movieCursor.moveToPosition(position))
            throw new IllegalStateException("Position invalid");
        return new Movie(movieCursor);
    }

    public class MovieViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_cover) ImageView ivCover;
        @BindView(R.id.tv_title) TextView tvTitle;
        @BindView(R.id.tv_overview) TextView tvOverview;
        @BindView(R.id.tv_date) TextView tvDate;
        @BindView(R.id.card_view)
        CardView cardView;

        public MovieViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
