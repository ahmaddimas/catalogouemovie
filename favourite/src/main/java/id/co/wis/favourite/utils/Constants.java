package id.co.wis.favourite.utils;

public class Constants {
    public static final String BASE_URL = "https://api.themoviedb.org/3/";
    public static final String IMAGE_BASE_URL = "https://image.tmdb.org/t/p/w185_and_h278_bestv2/";
    public static final String IMAGE_BACKDROP_BASE_URL = "https://image.tmdb.org/t/p/w350_and_h196_bestv2/";
}
