package id.co.wis.favourite.helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateHelper {
    public static String formatDate(String curFormat, String reqFormat, String d) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(curFormat, Locale.getDefault());
        Date date = null;

        try {
            date = simpleDateFormat.parse(d);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return new SimpleDateFormat(reqFormat, Locale.getDefault()).format(date);
    }
}
