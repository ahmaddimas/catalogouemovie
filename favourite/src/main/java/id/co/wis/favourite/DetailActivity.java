package id.co.wis.favourite;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.wis.favourite.db.MovieContract;
import id.co.wis.favourite.helper.DateHelper;
import id.co.wis.favourite.model.Movie;
import id.co.wis.favourite.utils.Constants;

public class DetailActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.iv_movie_backdrop) ImageView ivMovie;
    @BindView(R.id.tv_title) TextView tvTitle;
    @BindView(R.id.tv_date) TextView tvDate;
    @BindView(R.id.tv_overview) TextView tvOverview;
    @BindView(R.id.fab) FloatingActionButton btnFab;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    Movie movie;
    Uri uri = null;
    boolean available = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);

        if (getIntent().getExtras() != null) {
            movie = getIntent().getExtras().getParcelable("MOVIE");
        }
        uri = getIntent().getData();

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        if (movie != null) {
            String date = movie.getRelease_date().equalsIgnoreCase("") ? "Day Month Year" : DateHelper.formatDate("yyyy-MM-dd", "EEEE, MMM d, yyyy", movie.getRelease_date());
            Glide.with(this).load(Constants.IMAGE_BACKDROP_BASE_URL + movie.getBackdrop_path()).into(ivMovie);
            setTitle("");
            tvTitle.setText(movie.getTitle());
            tvDate.setText(date);
            tvOverview.setText(movie.getOverview());

            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            if (cursor != null && cursor.getCount() > 0) {
                available = true;
                changeImageButton(R.drawable.ic_favourite);
                cursor.close();
            }
        }

        btnFab.setOnClickListener(this);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onClick(View view) {
        if (movie != null && uri != null) {
            if (available) {
                int deleted = getContentResolver().delete(uri, null, null);
                Log.d("PROVIDER", "detail deleted: "+ deleted);
                if (deleted == 1) {
                    available = false;
                    changeImageButton(R.drawable.ic_favourite_o);
                    Snackbar.make(view, getString(R.string.remove_success_message), Snackbar.LENGTH_LONG).show();
                } else
                    Snackbar.make(view, getString(R.string.remove_failed_message), Snackbar.LENGTH_LONG).show();
            } else {
                ContentValues values = new ContentValues();

                values.put(MovieContract.EntityColumns._ID, movie.getId());
                values.put(MovieContract.EntityColumns.title, movie.getTitle());
                values.put(MovieContract.EntityColumns.releaseDate, movie.getRelease_date());
                values.put(MovieContract.EntityColumns.posterPath, movie.getPoster_path());
                values.put(MovieContract.EntityColumns.backdropPath, movie.getBackdrop_path());
                values.put(MovieContract.EntityColumns.overview, movie.getOverview());
                values.put(MovieContract.EntityColumns.popularity, movie.getPopularity());
                getContentResolver().insert(MovieContract.CONTENT_URI, values);

                available = true;
                changeImageButton(R.drawable.ic_favourite);
                Snackbar.make(view, getString(R.string.add_success_message), Snackbar.LENGTH_LONG).show();
            }
        } else {
            Snackbar.make(view, getString(R.string.unknown_message), Snackbar.LENGTH_LONG).show();
        }
    }

    private void changeImageButton(int resId) {
        btnFab.setImageResource(resId);
    }
}
